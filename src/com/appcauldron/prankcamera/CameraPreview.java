package com.appcauldron.prankcamera;

import java.io.IOException;
import java.util.ArrayList;

import com.appcauldron.prankcamera.R.anim;
import com.appcauldron.prankcamera.views.FaceTrackerView;
import com.appcauldron.prankcamera.views.ScaryPictureView;

import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

/** A basic Camera preview class */
public class CameraPreview extends SurfaceView implements
		SurfaceHolder.Callback {
	private SurfaceHolder mHolder;
	private Camera mCamera;
	private ArrayList<FaceTrackerView> mRectangleViews;
	private Context mContext;
	private FrameLayout mPreviewFrame;
	private Boolean mPrankOn;

	public void PerformPrank() {
		mPrankOn = true;
	}

	@SuppressWarnings("deprecation")
	public CameraPreview(Context context, Camera camera,
			FrameLayout previewFrame) {
		super(context);
		this.mCamera = camera;
		this.mContext = context;
		this.mPreviewFrame = previewFrame;
		this.mRectangleViews = new ArrayList<>();
		this.mPrankOn = false;

		this.mHolder = getHolder();
		this.mHolder.addCallback(this);

		// TODO: The surface needs scaling
		this.mHolder.setSizeFromLayout();
		// this.mHolder.setFixedSize(640, 480);
		// deprecated setting, but required on Android versions prior to 3.0
		this.mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the
		// preview.
		try {
			this.mCamera.setPreviewDisplay(holder);
			// mCamera.setFaceDetectionListener(faceDetectionListener);
			mCamera.startPreview();
			// this.mCamera.startFaceDetection();

		} catch (IOException e) {
			Log.d("Camera Preview",
					"Error setting camera preview: " + e.getMessage());
		}
	}

	public Camera.FaceDetectionListener faceDetectionListener = new Camera.FaceDetectionListener() {

		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {
			// for(Face face : faces){
			if (mPreviewFrame.getChildCount() > 1) {
				mPreviewFrame.removeViews(1, mPreviewFrame.getChildCount() - 1);
			}

			if (faces.length > 0) {
				Log.i("CameraPreview", "FacesFound: " + faces.length);

				if (mPrankOn) {
					mCamera.stopFaceDetection();
					for (Face face : faces) {
						RectF rectF = new RectF(face.rect);
						Matrix matrix = new Matrix();
						matrix.setScale(1, 1);
						matrix.postScale(mPreviewFrame.getWidth() / 2000f,
								mPreviewFrame.getHeight() / 2000f);
						matrix.postTranslate(mPreviewFrame.getWidth() / 2f,
								mPreviewFrame.getHeight() / 2f);
						matrix.mapRect(rectF);
						ScaryPictureView scaryPictureView = new ScaryPictureView(
								mContext, rectF);
						ScaleAnimation scaleAnimation=new ScaleAnimation(1, 3, 1, 3, rectF.centerX(), rectF.centerY());
						ScaleAnimation scaleAnimation1=new ScaleAnimation(3, 3, 3, 3, rectF.centerX(), rectF.centerY());
						scaleAnimation.setDuration(300);
						scaleAnimation1.setDuration(5000);
						AnimationSet animationSet=new AnimationSet(true);
						animationSet.addAnimation(scaleAnimation);
						animationSet.addAnimation(scaleAnimation1);
						mPreviewFrame.addView(scaryPictureView);
						scaryPictureView.startAnimation(animationSet);
					}
					MediaPlayer mediaPlayer = MediaPlayer.create(mContext, R.drawable.scream2);
					mediaPlayer.start();
					Vibrator vibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
					if(vibrator.hasVibrator()){
						vibrator.vibrate(500);
					}
					
					
					mPrankOn = false;
					mCamera.startFaceDetection();

				} else {
					for (Face face : faces) {
						RectF rectF = new RectF(face.rect);
						Matrix matrix = new Matrix();
						matrix.setScale(1, 1);
						matrix.postScale(mPreviewFrame.getWidth() / 2000f,
								mPreviewFrame.getHeight() / 2000f);
						matrix.postTranslate(mPreviewFrame.getWidth() / 2f,
								mPreviewFrame.getHeight() / 2f);
						matrix.mapRect(rectF);
						mPreviewFrame.addView(new FaceTrackerView(mContext,
								rectF));
						// mPreviewFrame.addView(new ScaryPictureView(mContext,
						// rectF));
					}
				}
			}
		}
	};

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.
		// mCamera.stopPreview();
		// mCamera.release();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.

		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}

		// set preview size and make any resize, rotate or
		// reformatting changes here

		// start preview with new settings
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
			mCamera.setFaceDetectionListener(faceDetectionListener);
			mCamera.startFaceDetection();

		} catch (Exception e) {
			// Log.d(TAG, "Error starting camera preview: " + e.getMessage());
		}
	}
}