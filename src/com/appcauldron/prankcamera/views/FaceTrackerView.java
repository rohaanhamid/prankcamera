package com.appcauldron.prankcamera.views;

import java.util.jar.Attributes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class FaceTrackerView extends View{
	
	private RectF mRectangle;
	private Paint mPaint = new Paint();

	public FaceTrackerView(Context context, RectF faceRect) {
		super(context);
		mPaint.setColor(Color.GREEN);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(3);
		mRectangle=faceRect;

	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawRect(mRectangle, mPaint);
		
	}
	
	public void setRectangle(RectF rect){
		this.mRectangle = rect; 
	}
	
	
}