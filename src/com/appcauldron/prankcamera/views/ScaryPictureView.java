package com.appcauldron.prankcamera.views;

import com.appcauldron.prankcamera.R;
import com.appcauldron.prankcamera.R.drawable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.widget.ImageView;

public class ScaryPictureView extends ImageView{
	
	private RectF mRect;
	private Bitmap mBitmap;

	public ScaryPictureView(Context context, RectF rect) {
		super(context);
		mRect=rect;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.scary3);
		mBitmap=Bitmap.createScaledBitmap(bitmap,(int) (mRect.right - mRect.left),(int) (-mRect.top + mRect.bottom), false);
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawBitmap(mBitmap, mRect.centerX() - (mBitmap.getWidth()/2), mRect.centerY() - (mBitmap.getHeight()/2), null);

	}
	
	public void setRectangle(RectF rect){
	}
	
	
}