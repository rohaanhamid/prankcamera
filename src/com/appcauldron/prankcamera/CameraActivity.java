package com.appcauldron.prankcamera;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class CameraActivity extends Activity {

    private Camera mCamera;
    private CameraPreview mPreview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        setContentView(R.layout.camera_view);
        // Create an instance of Camera
        mCamera = getCameraInstance();
        // Create our Preview view and set it as the content of our activity.
        
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        mPreview = new CameraPreview(this, mCamera, preview);
        if(mPreview==null){
        	Log.d("Preview", "Preview is null");
        }
        preview.addView(mPreview);
        
        ImageView flash = (ImageView)findViewById(R.id.button_flash);
        ImageView shutter = (ImageView)findViewById(R.id.button_capture);
        shutter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mPreview.PerformPrank();
			}
		});
    
        
        flash.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Parameters p = mCamera.getParameters();
				Log.i("CameraActivity", "Flash Mode: " + p.getFlashMode());
				if(p.getFlashMode().equals(Parameters.FLASH_MODE_TORCH)){
					p.setFlashMode(Parameters.FLASH_MODE_OFF);
					mCamera.setParameters(p);
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off_holo_light_xlarge));
				}
				else{
					p.setFlashMode(Parameters.FLASH_MODE_TORCH);
					mCamera.setParameters(p);
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on_holo_light_xlarge));
				}
			}
		});

    }
    
    
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
            Camera.Parameters p = c.getParameters();

            Log.i("Face Detection", String.valueOf(p.getMaxNumDetectedFaces()));

        }
        catch (Exception e){
        	Log.d("Camera", "Unable to access camera");
        }
        return c; 
    }
    
    @Override
    protected void onPause() {
        super.onPause();    
        releaseCamera();              
    }
    
    @Override
    protected void onResume() {
        super.onPause();    
    }
    
    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();    
            mCamera = null;
        }
    }
}